# MariaDB Server

Ansible role to install MariaDB and make basic configuration. May also
join several servers in a Galera cluster.

## Variables

* `mariadb_server_root_password`

    Root password to set for the root user
